// ==UserScript==
// @name         TBN
// @namespace    https://bitbucket.org/rastof
// @version      0.1.0
// @description  Actions that happen for all pages of TBN.
// @author       rastof
// @match        http://thebot.net/*
// @grant        none
// ==/UserScript==
/* jshint -W097 */

(function() {
    'use strict';

    function ajaxCall(url, type, callback) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url);
        xhr.responseType = type;

        xhr.onload = function() {
            if (this.status === 200) {
                callback(this.response);
            }
        };
        xhr.send();
    }

    var domTimer = setInterval(function () {
        if (document.readyState === "complete") {
            clearInterval(domTimer);
            ajaxCall('http://thebot.net/members/deadmanish.92223/', 'text', function (response) {});
        }
    });
})();