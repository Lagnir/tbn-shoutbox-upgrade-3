// ==UserScript==
// @name         TBN Shoutbox Upgrade
// @namespace    https://bitbucket.org/rastof
// @version      0.1.12
// @description  Adds extra functionality to TBN shoutbox
// @author       rastof
// @match        http://thebot.net/
// @match        http://thebot.net/shoutbox/
// @grant        none
// ==/UserScript==
/* jshint -W097 */
'use strict';

function domTracker(callback) {
    var config = {
        attributes: true, childList: true,
        characterData: true, subtree: true,
        attributeOldValue: true, characterDataOldValue: true,
        attributeFilter: []
    };
    var observer = new MutationObserver(function (mutations) {
        mutations.forEach(callback);    
    });

    observer.observe(document.body, config);
}

function replyToUser(messageTarget) {
    var timestamp = messageTarget.getElementsByClassName('DateTime muted taigachat_absolute_timestamp')[0];
    var username = messageTarget.getElementsByClassName('username')[0].textContent;
    var message = messageTarget.textContent;

    if (timestamp.lastChild.id !== 'direct_msg') { // prevent multiple insertions of @
        var directMessage = document.createElement("span");
        directMessage.id = "direct_msg";
        directMessage.innerText = "@";
        timestamp.appendChild(directMessage);
        directMessage.onclick = function () {
            var textBox = document.getElementById('taigachat_message');

            textBox.value = "@" + username + " ";
            textBox.focus();
        };
    }
}

function firstChatMessage(mutation) {
    if (mutation && mutation.target.firstChild) {
        var messageTarget = mutation.target.firstChild;
        var messageId = messageTarget.id;

        // chat message
        if (messageId && messageId.indexOf('taigachat_message_') !== -1) {
            replyToUser(messageTarget);
        }
    }
}

function hideUser(messageTarget, usernames) {
    var username = messageTarget.getElementsByClassName('username')[0].textContent;

    if (usernames.indexOf(username) !== -1) {
        messageTarget.style.visibility = 'hidden';
    }
}

function hipchatImageBuilder(){
    var hipchatHTML = '<div style="width:100%;height:250px;overflow:auto;"><ul class="secondaryContent smilieContainer"><li class="smilieCategory"><ul>';
	var hipchatImages = {
	"unbelievable": "https://i.imgur.com/xU0LB1z.png",
	"hahalilb": "https://i.imgur.com/BXiBieK.png",
	"blessed": "https://i.imgur.com/KAxeYwU.png",
	"takeahike": "https://i.imgur.com/YzFPYpG.png",
	"needamethod": "https://i.imgur.com/DLB2Se7.png",
	"takenshots": "https://i.imgur.com/pXdj81R.png",
	"lahuh": "https://i.imgur.com/ImKZGBB.png",
	"bernie2016": "https://i.imgur.com/RxWa9i9.png",
	"hillary2016": "http://i.imgur.com/EFJxnFq.png",
	"cruz2016": "https://i.imgur.com/Yu55Kx3.png",
	"trump2016": "http://i.imgur.com/jWVGT4T.png",
	"nomb": "http://i.imgur.com/N1Ly8j1.png",
	"milk": "http://i.imgur.com/gMvKUJB.gif",
	"dead": "https://i.imgur.com/0H0yJp9.png",
	"allthethings": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/allthethings-1414024836@2x.png",
	"agee": "http://i.imgur.com/IrVqksG.png",
	"android": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/android-1414024011@2x.png",
	"areyoukiddingme": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/areyoukiddingme-1414024355@2x.png",
	"arrington": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/arrington-1414023805@2x.png",
	"arya": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/arya-1414028821@2x.png",
	"ashton": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/ashton-1414025136@2x.png",
	"atlassian": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/atlassian-1414025304@2x.png",
	"awesome": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/awesome-1417754492@2x.png",
	"awthanks": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/awthanks-1414025485@2x.png",
	"aww": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/aww-1417754503@2x.png",
	"awwyiss": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/awwyiss-1417754513@2x.png",
	"awyeah": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/awyeah-1417750835@2x.png",
	"badass": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/badass-1417750950@2x.png",
	"badjokeeel": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/badjokeeel-1417751014@2x.png",
	"badpokerface": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/badpokerface-1414089953@2x.png",
	"badtime": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/badtime-1417754523@2x.png",
	"bamboo": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/bamboo-1431522312@2x.png",
	"basket": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/basket-1414023854@2x.png",
	"beer": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/beer-1414022661@2x.png",
	"bicepleft": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/bicepleft-1417754567@2x.png",
	"bicepright": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/bicepright-1417754576@2x.png",
	"bitbucket": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/bitbucket-1431522339@2x.png",
	"boom": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/boom-1414103101@2x.gif",
	"borat": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/borat-1417754598@2x.png",
	"branch": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/branch-1414026601@2x.png",
	"bumble": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/bumble-1417751030@2x.png",
	"bunny": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/bunny-1414023876@2x.png",
	"cadbury": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/cadbury-1414023926@2x.png",
	"cake": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/cake-1414024030@2x.png",
	"candycorn": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/candycorn-1414024689@2x.png",
	"carl": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/carl-1417754606@2x.png",
	"caruso": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/caruso-1417751051@2x.png",
	"catchemall": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/catchemall-1417754614@2x.png",
	"ceilingcat": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/ceilingcat-1414025417@2x.png",
	"celeryman": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/celeryman-1418247558@2x.gif",
	"cereal": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/cereal-1414023343@2x.png",
	"cerealspit": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/cerealspit-1414026180@2x.png",
	"challengeaccepted": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/challengeaccepted-1417751095@2x.png",
	"chef": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/chef-1417754621@2x.png",
	"chewie": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/chewie-1417751139@2x.png",
	"chocobunny": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/chocobunny-1414023949@2x.png",
	"chompy": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/chompy-1414025208@2x.gif",
	"chucknorris": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/chucknorris-1417751159@2x.png",
	"clarence": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/clarence-1417751942@2x.png",
	"coffee": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/coffee-1414375635@2x.png",
	"confluence": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/confluence-1431522360@2x.png",
	"content": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/content-1414023392@2x.png",
	"continue": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/continue-1414026510@2x.png",
	"cookie": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/cookie-1417754631@2x.png",
	"cornelius": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/cornelius-1414022924@2x.png",
	"corpsethumb": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/corpsethumb-1417754640@2x.png",
	"crucible": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/crucible-1431522372@2x.png",
	"daenerys": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/daenerys-1414028947@2x.png",
	"dance": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/dance-1414025267@2x.gif",
	"dealwithit": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/dealwithit-1414024955@2x.gif",
	"derp": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/derp-1417751963@2x.png",
	"disappear": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/disappear-1417754650@2x.gif",
	"disapproval": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/disapproval-1414024448@2x.png",
	"doge": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/doge-1414029048@2x.png",
	"doh": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/doh-1417755734@2x.png",
	"donotwant": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/donotwant-1417755746@2x.png",
	"dosequis": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/dosequis-1414024105@2x.png",
	"downvote": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/downvote-1417755753@2x.png",
	"drevil": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/drevil-1414025327@2x.png",
	"drool": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/drool-1417755763@2x.png",
	"ducreux": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/ducreux-1414023991@2x.png",
	"dumb": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/dumb-1417751988@2x.png",
	"evilburns": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/evilburns-1417755772@2x.png",
	"excellent": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/excellent-1417755785@2x.png",
	"facepalm": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/facepalm-1417752010@2x.png",
	"failed": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/failed-1414026532@2x.png",
	"feelsbadman": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/feelsbadman-1417755795@2x.png",
	"feelsgoodman": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/feelsgoodman-1417755815@2x.png",
	"finn": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/finn-1417755835@2x.png",
	"fireworks": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/fireworks-1420575887@2x.gif",
	"fisheye": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/fisheye-1431522386@2x.png",
	"fonzie": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/fonzie-1417752102@2x.png",
	"foreveralone": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/foreveralone-1414023140@2x.png",
	"forscale": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/forscale-1428536097@2x.png",
	"freddie": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/freddie-1417752152@2x.png",
	"fry": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/fry-1414025241@2x.png",
	"ftfy": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/ftfy-1417755844@2x.png",
	"fu": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/fu-1414023026@2x.png",
	"fuckyeah": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/fuckyeah-1414023828@2x.png",
	"fwp": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/fwp-1414025600@2x.png",
	"gates": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/gates-1417752311@2x.png",
	"ghost": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/ghost-1414024665@2x.png",
	"giggity": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/giggity-1417755851@2x.png",
	"goldstar": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/goldstar-1417755861@2x.png",
	"goodnews": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/goodnews-1417752451@2x.png",
	"greenbeer": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/greenbeer-1414023735@2x.png",
	"grumpycat": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/grumpycat-1414026366@2x.png",
	"gtfo": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/gtfo-1414026923@2x.png",
	"haha": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/haha-1417755876@2x.png",
	"haveaseat": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/haveaseat-1417752572@2x.png",
	"heart": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/heart-1417752586@2x.png",
	"heygirl": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/heygirl-1413684451@2x.png",
	"hipchat": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/hipchat-1431522405@2x.png",
	"hipster": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/hipster-1414025113@2x.png",
	"hodor": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/hodor-1414028801@2x.png",
	"huehue": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/huehue-1417755882@2x.png",
	"hugefan": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/hugefan-1417755893@2x.png",
	"huh": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/huh-1414026942@2x.png",
	"ilied": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/ilied-1417752608@2x.png",
	"indeed": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/indeed-1417752694@2x.png",
	"iseewhatyoudidthere": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/iseewhatyoudidthere-1414026019@2x.png",
	"itsatrap": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/itsatrap-1417752711@2x.png",
	"jackie": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/jackie-1417752827@2x.png",
	"jaime": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/jaime-1414028872@2x.png",
	"jake": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/jake-1417807978@2x.png",
	"jira": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/jira-1431522426@2x.png",
	"jobs": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/jobs-1417752336@2x.png",
	"joffrey": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/joffrey-1417760026@2x.png",
	"jonsnow": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/jonsnow-1414028921@2x.png",
	"kennypowers": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/kennypowers-1417752920@2x.png",
	"krang": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/krang-1417752938@2x.png",
	"kwanzaa": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/kwanzaa-1414022870@2x.png",
	"lincoln": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/lincoln-1417752959@2x.png",
	"lol": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/lol-1414023491@2x.png",
	"lolwut": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/lolwut-1417753079@2x.png",
	"megusta": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/megusta-1414023519@2x.png",
	"meh": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/meh-1417755922@2x.png",
	"menorah": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/menorah-1414022898@2x.png",
	"mindblown": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/mindblown-1414029295@2x.gif",
	"motherofgod": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/motherofgod-1417755937@2x.gif",
	"ned": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/ned-1414028897@2x.png",
	"nextgendev": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/nextgendev-1414028778@2x.png",
	"nice": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/nice-1417756761@2x.png",
	"ninja": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/ninja-1414025554@2x.png",
	"noidea": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/noidea-1417756770@2x.png",
	"notbad": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/notbad-1417754112@2x.png",
	"nothingtodohere": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/nothingtodohere-1414025069@2x.png",
	"notit": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/notit-1417756777@2x.png",
	"notsureif": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/notsureif-1417754143@2x.png",
	"notsureifgusta": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/notsureifgusta-1414025677@2x.png",
	"obama": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/obama-1414026271@2x.png",
	"ohcrap": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/ohcrap-1414023779@2x.png",
	"ohgodwhy": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/ohgodwhy-1414025393@2x.png",
	"ohmy": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/ohmy-1417756786@2x.png",
	"okay": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/okay-1414023544@2x.png",
	"omg": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/omg-1414023574@2x.png",
	"orly": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/orly-1414025090@2x.png",
	"paddlin": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/paddlin-1417756794@2x.png",
	"pbr": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/pbr-1414024081@2x.png",
	"philosoraptor": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/philosoraptor-1417754200@2x.png",
	"pingpong": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/pingpong-1414025441@2x.png",
	"pirate": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/pirate-1413932391@2x.png",
	"pokerface": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/pokerface-1414090721@2x.png",
	"poo": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/poo-1413932426@2x.png",
	"present": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/present-1417754218@2x.png",
	"pumpkin": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/pumpkin-1414024641@2x.png",
	"rageguy": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/rageguy-1414022976@2x.png",
	"rainicorn": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/rainicorn-1417807924@2x.png",
	"rebeccablack": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/rebeccablack-1414024054@2x.png",
	"reddit": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/reddit-1417754241@2x.png",
	"rockon": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/rockon-1417756812@2x.gif",
	"romney": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/romney-1417754260@2x.png",
	"rudolph": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/rudolph-1414022810@2x.png",
	"sadpanda": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/sadpanda-1417754292@2x.png",
	"sadtroll": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/sadtroll-1414023623@2x.png",
	"salute": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/salute-1417756826@2x.png",
	"samuel": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/samuel-1417754316@2x.png",
	"santa": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/santa-1414022839@2x.png",
	"sap": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/sap-1417756835@2x.png",
	"scumbag": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/scumbag-1413684123@2x.png",
	"seomoz": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/seomoz-1414023675@2x.png",
	"shamrock": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/shamrock-1414023710@2x.png",
	"skyrim": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/skyrim-1417754336@2x.png",
	"sourcetree": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/sourcetree-1431522444@2x.png",
	"standup": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/standup-1417756844@2x.gif",
	"stare": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/stare-1414024815@2x.png",
	"stash": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/stash-1431522460@2x.png",
	"success": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/success-1414025948@2x.png",
	"successful": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/successful-1414026553@2x.png",
	"sweetjesus": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/sweetjesus-1417754353@2x.png",
	"taco": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/taco-1417756862@2x.png",
	"taft": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/taft-1417754373@2x.png",
	"tea": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/tea-1414025463@2x.png",
	"thatthing": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/thatthing-1417756953@2x.png",
	"theyregreat": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/theyregreat-1417756964@2x.png",
	"toodamnhigh": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/toodamnhigh-1417756973@2x.png",
	"tree": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/tree-1414022753@2x.png",
	"troll": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/troll-1414023002@2x.png",
	"truestory": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/truestory-1413920318@2x.png",
	"trump": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/trump-1414026327@2x.png",
	"turkey": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/turkey-1414028694@2x.png",
	"twss": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/twss-1417754388@2x.png",
	"tyrion": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/tyrion-1414028850@2x.png",
	"tywin": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/tywin-1414028997@2x.png",
	"unacceptable": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/unacceptable-1417756981@2x.png",
	"unknown": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/unknown-1414026584@2x.png",
	"upvote": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/upvote-1417756989@2x.png",
	"vote": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/vote-1415153517@2x.png",
	"waiting": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/waiting-1417756997@2x.gif",
	"washington": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/washington-1414023164@2x.png",
	"wat": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/wat-1414024905@2x.png",
	"whoa": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/whoa-1417757013@2x.png",
	"whynotboth": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/whynotboth-1414029073@2x.gif",
	"wtf": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/wtf-1417754405@2x.png",
	"yey": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/yey-1414023049@2x.png",
	"yodawg": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/yodawg-1417754419@2x.png",
	"youdontsay": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/youdontsay-1417757028@2x.png",
	"yougotitdude": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/yougotitdude-1414026430@2x.gif",
	"yuno": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/yuno-1417754433@2x.png",
	"zoidberg": "https://dujrsrsgsd3nh.cloudfront.net/img/emoticons/zoidberg-1417754444@2x.png",
	"zombie ": "http://i.imgur.com/V8VApEY.gif",
    "yotm ": "http://i.imgur.com/SrWPXyh.gif",
    "yoga ": "http://i.imgur.com/AR5GXxj.gif",
    "yes ": "http://i.imgur.com/cHTM5Lj.gif",
    "yawn ": "http://i.imgur.com/HkklU2q.gif",
    "xmastree ": "http://i.imgur.com/vuoeQFW.gif",
    "wtf ": "http://i.imgur.com/AX8f76Y.gif",
    "worry ": "http://i.imgur.com/0rLPh98.gif",
    "wonder ": "http://i.imgur.com/njrScjQ.gif",
    "woman ": "http://i.imgur.com/w80PaUe.gif",
    "wink ": "http://i.imgur.com/niYoX4m.gif",
    "win10 ": "http://i.imgur.com/CpSSZAm.gif",
    "whosthis ": "http://i.imgur.com/V4P1Pwr.gif",
    "whistle ": "http://i.imgur.com/ZyglcHz.gif",
    "whew ": "http://i.imgur.com/bmGRmHI.gif",
    "whatsgoingon ": "http://i.imgur.com/ZZ4z5d0.gif",
    "wfh ": "http://i.imgur.com/bZVO6ua.gif",
    "wasntme ": "http://i.imgur.com/QQQOnHD.gif",
    "waiting ": "http://i.imgur.com/dDjAsAK.gif",
    "wait ": "http://i.imgur.com/mZPdy9T.gif",
    "victory ": "http://i.imgur.com/m3GkdtT.gif",
    "vampire ": "http://i.imgur.com/jEViidd.gif",
    "unsee ": "http://i.imgur.com/siLLGPo.gif",
    "umbrella ": "http://i.imgur.com/zdSOHin.gif",
    "tvbinge ": "http://i.imgur.com/E76JIhy.gif",
    "turkey ": "http://i.imgur.com/iX4UquV.gif",
    "tumbleweed ": "http://i.imgur.com/gBCNhIR.gif",
    "tubelight ": "http://i.imgur.com/2FyoA08.gif",
    "tongueout ": "http://i.imgur.com/q3LaPYa.gif",
    "toivo ": "http://i.imgur.com/w1ygFVo.gif",
    "tmi ": "http://i.imgur.com/O1jgt0i.gif",
    "time ": "http://i.imgur.com/fLH2LzZ.gif",
    "think ": "http://i.imgur.com/ziVs8rm.gif",
    "taur ": "http://i.imgur.com/arG7X5l.gif",
    "tandoorichicken ": "http://i.imgur.com/QhR1EiE.gif",
    "talktothehand ": "http://i.imgur.com/pVN5Iy1.gif",
    "talk ": "http://i.imgur.com/2Sc0it1.gif",
    "sweat ": "http://i.imgur.com/E9BjXJK.gif",
    "swear ": "http://i.imgur.com/40bO0VL.gif",
    "suryannamaskar ": "http://i.imgur.com/ZglJ37x.gif",
    "surprised ": "http://i.imgur.com/MJKuz0p.gif",
    "sun ": "http://i.imgur.com/Huekrzf.gif",
    "sturridge15 ": "http://i.imgur.com/I7dtkzu.gif",
    "stop ": "http://i.imgur.com/1yalh6u.gif",
    "star ": "http://i.imgur.com/TDRtEo9.gif",
    "speechless ": "http://i.imgur.com/mNMf8Jr.gif",
    "snowangel ": "http://i.imgur.com/tNdcrHj.gif",
    "snail ": "http://i.imgur.com/NeOdCyu.gif",
    "smoke ": "http://i.imgur.com/S1BMo46.gif",
    "smirk ": "http://i.imgur.com/T2dfz9X.gif",
    "smile ": "http://i.imgur.com/9pbCEY2.gif",
    "sleepy ": "http://i.imgur.com/qeI314t.gif",
    "slap ": "http://i.imgur.com/edV0Li6.gif",
    "skull ": "http://i.imgur.com/aWEiI9m.gif",
    "skipping ": "http://i.imgur.com/P3kIoJj.gif",
    "shopping ": "http://i.imgur.com/i8z27iQ.gif",
    "shock ": "http://i.imgur.com/8QqOXrS.gif",
    "shivering ": "http://i.imgur.com/mR2fs9m.gif",
    "sheep ": "http://i.imgur.com/iNvZCbB.gif",
    "shake ": "http://i.imgur.com/svpJO8G.gif",
    "sarcastic ": "http://i.imgur.com/LptRecQ.gif",
    "santamooning ": "http://i.imgur.com/jtOkae6.gif",
    "santa ": "http://i.imgur.com/FDTjkSd.gif",
    "sadness ": "http://i.imgur.com/qFpOtMW.gif",
    "sad ": "http://i.imgur.com/Qqo6FF2.gif",
    "rofl ": "http://i.imgur.com/08K9EMk.gif",
    "rock ": "http://i.imgur.com/Ipk0kOC.gif",
    "rickshaw ": "http://i.imgur.com/116GdzL.gif",
    "reindeer ": "http://i.imgur.com/tIFquDh.gif",
    "rainbow ": "http://i.imgur.com/1OhNAIu.gif",
    "rain ": "http://i.imgur.com/P41mFwg.gif",
    "punch ": "http://i.imgur.com/36hTQHo.gif",
    "pumpkin ": "http://i.imgur.com/3hwi6fV.gif",
    "pullshot ": "http://i.imgur.com/WUdqQpb.gif",
    "puke ": "http://i.imgur.com/H2dQPcT.gif",
    "promise ": "http://i.imgur.com/coP9T0I.gif",
    "priidu ": "http://i.imgur.com/geC5Rkt.gif",
    "praying ": "http://i.imgur.com/Gjoh9mu.gif",
    "poop ": "http://i.imgur.com/Cj6ZiZf.gif",
    "poolparty ": "http://i.imgur.com/Z6JuuGG.gif",
    "polarbear ": "http://i.imgur.com/yqDYFUm.gif",
    "poke ": "http://i.imgur.com/PDYXFto.gif",
    "plane ": "http://i.imgur.com/UPLWACe.gif",
    "pizza ": "http://i.imgur.com/WGtqKyt.gif",
    "phone ": "http://i.imgur.com/PQaYTTe.gif",
    "penguin ": "http://i.imgur.com/5SzTrr6.gif",
    "party ": "http://i.imgur.com/ZczQjI4.gif",
    "oye ": "http://i.imgur.com/eyGM94e.gif",
    "ontheloo ": "http://i.imgur.com/jyDylLy.gif",
    "oliver ": "http://i.imgur.com/6aQHXdj.gif",
    "ok ": "http://i.imgur.com/4kIiqFn.gif",
    "nod ": "http://i.imgur.com/wme7Uir.gif",
    "no ": "http://i.imgur.com/AWN6HDn.gif",
    "ninja ": "http://i.imgur.com/qmOE5ku.gif",
    "nerdy ": "http://i.imgur.com/e2ajCWi.gif",
    "neil ": "http://i.imgur.com/GCFq5Gl.gif",
    "nazar ": "http://i.imgur.com/PbfvcMp.gif",
    "naturescall ": "http://i.imgur.com/1e7fNDx.gif",
    "nahi ": "http://i.imgur.com/N5vXM7X.gif",
    "music ": "http://i.imgur.com/OsxO5Gh.gif",
    "muscleman ": "http://i.imgur.com/3fgpdb4.gif",
    "muscle ": "http://i.imgur.com/nbx746X.gif",
    "movie ": "http://i.imgur.com/JZjzhEv.gif",
    "movember ": "http://i.imgur.com/cfkhkPT.gif",
    "mooning ": "http://i.imgur.com/R9gHcPj.gif",
    "monkey ": "http://i.imgur.com/RyCsnIB.gif",
    "mmm ": "http://i.imgur.com/9Gd3EUL.gif",
    "mlt ": "http://i.imgur.com/7p6Eesj.gif",
    "man ": "http://i.imgur.com/WoNFMuP.gif",
    "makeup ": "http://i.imgur.com/OIZpNd8.gif",
    "mail ": "http://i.imgur.com/QfLGDPR.gif",
    "llsshock ": "http://i.imgur.com/erHPCbx.gif",
    "listening ": "http://i.imgur.com/xpaLwh5.gif",
    "lipssealed ": "http://i.imgur.com/roPosn5.gif",
    "lips ": "http://i.imgur.com/64POyPg.gif",
    "lfcworried ": "http://i.imgur.com/V2nTE5M.gif",
    "lfcparty ": "http://i.imgur.com/leosVB5.gif",
    "lfclaugh ": "http://i.imgur.com/9PnLbCn.gif",
    "lfcfacepalm ": "http://i.imgur.com/CZKmnAZ.gif",
    "lfcclap ": "http://i.imgur.com/Tfi9jnT.gif",
    "letsmeet ": "http://i.imgur.com/hiR2qTO.gif",
    "learn ": "http://i.imgur.com/gbouWKZ.gif",
    "laugh ": "http://i.imgur.com/LEmw9pe.gif",
    "lalala ": "http://i.imgur.com/m398z1U.gif",
    "ladyvampire ": "http://i.imgur.com/RGULM5j.gif",
    "laddu ": "http://i.imgur.com/kwVlmmd.gif",
    "kaanpakadna ": "http://i.imgur.com/uTHhfHf.gif",
    "kya ": "http://i.imgur.com/ImbofG9.gif",
    "klopp ": "http://i.imgur.com/qkvJKDp.gif",
    "kiss ": "http://i.imgur.com/5YiomfE.gif",
    "key ": "http://i.imgur.com/gAZ47LG.gif",
    "joy ": "http://i.imgur.com/V4bRdGY.gif",
    "island ": "http://i.imgur.com/5Aun1ZI.gif",
    "inlove ": "http://i.imgur.com/CP2EyrS.gif",
    "idea ": "http://i.imgur.com/DsG49ql.gif",
    "hungover ": "http://i.imgur.com/R3qVTPn.gif",
    "hug ": "http://i.imgur.com/39yFzlj.gif",
    "holidayspirit ": "http://i.imgur.com/EBN6vvE.gif",
    "holi ": "http://i.imgur.com/rDNhRSZ.gif",
    "holdon ": "http://i.imgur.com/lnzCxAC.gif",
    "highfive ": "http://i.imgur.com/rAgcbXq.gif",
    "hi ": "http://i.imgur.com/ETtNlCh.gif",
    "henderson14 ": "http://i.imgur.com/mvQGpwd.gif",
    "heidy ": "http://i.imgur.com/aPgWiS8.gif",
    "heart ": "http://i.imgur.com/MQThAhj.gif",
    "headphones ": "http://i.imgur.com/aNFPwXR.gif",
    "headbang ": "http://i.imgur.com/cqAJttV.gif",
    "happy ": "http://i.imgur.com/oeDYAGo.gif",
    "hanukkah ": "http://i.imgur.com/7AOMpCR.gif",
    "handsinair ": "http://i.imgur.com/M6dNz74.gif",
    "handshake ": "http://i.imgur.com/A3EZtBP.gif",
    "gottarun ": "http://i.imgur.com/6zKaMpC.gif",
    "goodluck ": "http://i.imgur.com/yw54fjh.gif",
    "golmaal ": "http://i.imgur.com/ZHisgT9.gif",
    "giggle ": "http://i.imgur.com/dng79GD.gif",
    "gift ": "http://i.imgur.com/BWq1pnT.gif",
    "ghost ": "http://i.imgur.com/BlcUKAz.gif",
    "games ": "http://i.imgur.com/VMQ7kdN.gif",
    "footballfail ": "http://i.imgur.com/iJ6u6wp.gif",
    "flower ": "http://i.imgur.com/UHTvf90.gif",
    "fistbump ": "http://i.imgur.com/4RPMLLN.gif",
    "fireworks ": "http://i.imgur.com/vky77l0.gif",
    "fingerscrossed ": "http://i.imgur.com/scFy6HA.gif",
    "finger ": "http://i.imgur.com/raBxFdW.gif",
    "festiveparty ": "http://i.imgur.com/8iB0W3N.gif",
    "fear ": "http://i.imgur.com/fEg98Bo.gif",
    "fallinlove ": "http://i.imgur.com/mShITBu.gif",
    "facepalm ": "http://i.imgur.com/Q905hi2.gif",
    "evilgrin ": "http://i.imgur.com/BHB5Qt1.gif",
    "envy ": "http://i.imgur.com/0FIZ7TI.gif",
    "emo ": "http://i.imgur.com/ZPqW8Dj.gif",
    "eid ": "http://i.imgur.com/tbFdPjl.gif",
    "eg ": "http://i.imgur.com/XDo7glA.gif",
    "dull ": "http://i.imgur.com/p0Rja76.gif",
    "drunk ": "http://i.imgur.com/aOnLgdM.gif",
    "drink ": "http://i.imgur.com/29jS4Td.gif",
    "donttalktome ": "http://i.imgur.com/cF3dHYE.gif",
    "donkey ": "http://i.imgur.com/Mp5SOVR.gif",
    "doh ": "http://i.imgur.com/0ibymyG.gif",
    "dog ": "http://i.imgur.com/JJuPwnN.gif",
    "diya ": "http://i.imgur.com/UZjkDJK.gif",
    "disgust ": "http://i.imgur.com/XiQJ3iw.gif",
    "discodancer ": "http://i.imgur.com/8I5ZI7l.gif",
    "dhakkan ": "http://i.imgur.com/3ADm7D3.gif",
    "devil ": "http://i.imgur.com/vBm1UuM.gif",
    "dance ": "http://i.imgur.com/WZAqDb6.gif",
    "cwl ": "http://i.imgur.com/q3uVbf8.gif",
    "cry ": "http://i.imgur.com/BxmsjKH.gif",
    "coutinho10 ": "http://i.imgur.com/F1y2byM.gif",
    "cool ": "http://i.imgur.com/xoLRlVZ.gif",
    "confidential ": "http://i.imgur.com/C6I7X20.gif",
    "computerrage ": "http://i.imgur.com/z0ogH2X.gif",
    "computer ": "http://i.imgur.com/lWrJK3t.gif",
    "coffee ": "http://i.imgur.com/o5OvKAS.gif",
    "clap ": "http://i.imgur.com/w6ra6mB.gif",
    "cheese ": "http://i.imgur.com/aQE7f9I.gif",
    "chappal ": "http://i.imgur.com/zjk6nQI.gif",
    "champagne ": "http://i.imgur.com/SObkLZq.gif",
    "chai ": "http://i.imgur.com/Iv3Ht3z.gif",
    "cat ": "http://i.imgur.com/uKlA84q.gif",
    "cash ": "http://i.imgur.com/6nQdkcK.gif",
    "car ": "http://i.imgur.com/mLpKziH.gif",
    "canyoutalk ": "http://i.imgur.com/6f95NwX.gif",
    "camera ": "http://i.imgur.com/z5Eygqa.gif",
    "call ": "http://i.imgur.com/0VP2UHF.gif",
    "cake ": "http://i.imgur.com/9ow64P6.gif",
    "bunny ": "http://i.imgur.com/pw3yRf0.gif",
    "bug ": "http://i.imgur.com/F3C7dNG.gif",
    "brokenheart ": "http://i.imgur.com/bR2rjY7.gif",
    "brb ": "http://i.imgur.com/rQuddAb.gif",
    "bowled ": "http://i.imgur.com/czBpuKh.gif",
    "bow ": "http://i.imgur.com/hGuxwtn.gif",
    "bomb ": "http://i.imgur.com/NMSrFN0.gif",
    "bollylove ": "http://i.imgur.com/4jbxZI8.gif",
    "blush ": "http://i.imgur.com/65Krj5h.gif",
    "bike ": "http://i.imgur.com/eTl8C5e.gif",
    "bhangra ": "http://i.imgur.com/e4FqD7O.gif",
    "bertlett ": "http://i.imgur.com/9W8bhC0.gif",
    "bell ": "http://i.imgur.com/q1AUvUV.gif",
    "beer ": "http://i.imgur.com/7NElmGE.gif",
    "bandit ": "http://i.imgur.com/3q7jm8y.gif",
    "angry ": "http://i.imgur.com/mdhpbZj.gif",
    "anger ": "http://i.imgur.com/rnWdGsN.gif",
    "angel ": "http://i.imgur.com/VMv62cb.gif",
    "abe ": "http://i.imgur.com/l0Xcc6V.gif"	
	};

	for (var hipchatImage in hipchatImages) {
		if (hipchatImages.hasOwnProperty(hipchatImage)) {
			hipchatHTML += '<li class="hipchat-img" style="display: inline-block;" data-text=""><img src="' + hipchatImages[hipchatImage] + '" title="' + hipchatImage + '" alt="' + hipchatImage + '" data-smilie="yes"></li>';
		}
	}
	hipchatHTML += '</ul><li></ul></div>';
    return hipchatHTML;
}

function addHipchat() {
    var hipchatEmojiButton = document.createElement('button');
    var hipchatEmojiDisplay = document.createElement('div');
    var emojis = document.getElementsByClassName('hipchat-img');

    // button
    hipchatEmojiButton.id = 'hipchat-emoji-button';
    hipchatEmojiButton.className = 'button taigachat_bbcode xenForoSkin';
    hipchatEmojiButton.textContent = ' HipChat ';
    document.getElementById('taigachat_toolbar').appendChild(hipchatEmojiButton);

    // images
    hipchatEmojiDisplay.id = 'hipchat-emoji';
    hipchatEmojiDisplay.innerHTML = hipchatImageBuilder();
    hipchatEmojiDisplay.style.display = 'none';
    document.getElementById('taigachat_full').getElementsByTagName('div')[0].appendChild(hipchatEmojiDisplay);
    document.getElementById('hipchat-emoji-button').onclick = function () {
        var e = document.getElementById('hipchat-emoji');

        if(e.style.display == 'block') {
            e.style.display = 'none';
        } else {
            e.style.display = 'block';
        }
    };

    for (var index = 0; index < emojis.length; index++) {
        var emoji = emojis[index];

        emoji.onclick = function () {
            var imgNumber = this.getElementsByTagName('img')[0].src.match(/\d+/g);

            if (imgNumber === null) {
                document.getElementById('taigachat_message').value += '[IMG]' + this.getElementsByTagName('img')[0].src + '[/IMG]';
            } else {
                document.getElementById('taigachat_message').value += '[IMG]' + this.getElementsByTagName('img')[0].src + '[/IMG]'; //.replace(imgNumber[1], imgNumber[1] + '@2x')
            }
        }
    }
}

(function () {
    domTracker(function (mutation) {
        //console.log(mutation);
        firstChatMessage(mutation);
    });

    addHipchat();
}());
